require "test_helper"

class EntityValuesHelperTest < ActionDispatch::IntegrationTest
  include EntityValuesHelper

  test "should get value by field and row" do
    entity_field = entity_fields(:one)
    entity_row = entity_rows(:one)
    assert_equal 'Focus', display_value_by_field_and_row(field: entity_field, row: entity_row)
  end
end
