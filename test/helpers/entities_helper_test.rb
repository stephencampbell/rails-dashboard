require "test_helper"

class EntitiesHelperTest < ActionDispatch::IntegrationTest
  include EntitiesHelper

  test "should build values indexed by field in an array of rows" do
    ford = entity_rows(:one)
    renault = entity_rows(:two)
    values = {
      ford.id => {'Model' => 'Focus', 'Make' => 'Ford', 'MPG' => '35', '0-60 (s)' => '14'},
      renault.id => {'Model' => 'Clio', 'Make' => 'Renault', 'MPG' => '40', '0-60 (s)' => '12'},
    }
    entity = entities(:one)
    assert_equal values, entity_values_by_row_and_field(entity)
  end
end
