require "test_helper"

class EntityGraphsHelperTest < ActionDispatch::IntegrationTest
  include EntityGraphsHelper

  test "should find data points" do
    values = [35.0, 40.0]
    entity_graph = entity_graphs(:one)
    data_points = get_data_points(entity_graph)
    assert_equal values.sort, data_points.sort
  end

  test "should find chart labels" do
    entity_graph = entity_graphs(:one)
    entity = entity_graph.entity
    field = entity.entity_fields.first
    values = entity.entity_values.filter {|value| value.entity_field == field}.map {|value| value.value}

    chart_labels = get_chart_labels(entity_graph)
    assert_equal values.sort, chart_labels.sort
  end
end
