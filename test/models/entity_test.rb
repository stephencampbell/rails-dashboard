require "test_helper"

class EntityTest < ActiveSupport::TestCase
  test "should bulk create row with values" do
    # Input data
    values_by_field = {
      'Make' => 'Honda',
      'Model' => 'Civic'
    }

    # Load entity to add row to
    entity = entities(:one)

    # Run function to test
    row = entity.update_row values_by_field: values_by_field

    # Create array of values to compare input to
    entity_values = EntityValue.where(entity_row: row).map { |entity_value| entity_value.value }

    # Compare input to actual values
    assert_equal values_by_field.values.sort, entity_values.sort, 'Written values do not match input values'
  end

  test "should bulk update row with values" do
    # Input data
    values_by_field = {
      'Make' => 'Seat',
      'Model' => 'Leon',
      'MPG' => '50',
      '0-60 (s)' => '11'
    }

    # Load entity and row to add values to
    entity = entities(:one)
    row = entity_rows(:one)

    # Run function to test
    entity.update_row(row:, values_by_field:)

    # Create array of values to compare input to
    entity_values = row.entity_values.map { |entity_value| entity_value.value }

    # Compare input to actual values
    assert_equal values_by_field.values.sort, entity_values.sort, 'Written values do not match input values'
  end

  test "should bulk update row values even if some are not set" do
    # Input data
    values_by_field = {
      'Model' => 'Golf',
      'Make' => 'Volkswagen'
    }

    # Comparison data
    comparison_data = {
      'Model' => 'Golf',
      'Make' => 'Volkswagen',
      'MPG' => '35',
      '0-60 (s)' => '14'
    }

    # Load entity and row to add values to
    entity = entities(:one)
    row = entity_rows(:one)

    # Run function to test
    entity.update_row(row:, values_by_field:)

    # Create array of values to compare input to
    entity_values = row.entity_values.map { |entity_value| entity_value.value }

    # Compare input to actual values
    assert_equal comparison_data.values.sort, entity_values.sort, 'Written values do not match input values'
  end

  test "should create a new row even if some values are not set" do
    # Input data
    values_by_field = {
      'Make' => 'Honda'
    }

    # Load entity to add row to
    entity = entities(:one)

    # Run function to test
    row = entity.update_row(values_by_field:)

    # Create array of values to compare input to
    entity_values = EntityValue.where(entity_row: row).map { |entity_value| entity_value.value }

    # Compare input to actual values
    assert_equal values_by_field.values.sort, entity_values.sort, 'Written values do not match input values'
  end

  test "should fail to update a row if no values are set" do
    # Input data
    values_by_field = {}

    # Load entity and row to add values to
    entity = entities(:one)
    row = entity_rows(:one)

    # Compare input to actual values
    assert_raises(UnpopulatedRowError) {
      entity.update_row(row:, values_by_field:)
    }
  end

  test "should fail to create a new row if no values are set" do
    # Input data
    values_by_field = {}

    # Load entity to add row to
    entity = entities(:one)

    # Run function to test
    assert_raises(UnpopulatedRowError) {
      entity.update_row values_by_field:
    }
  end
end
