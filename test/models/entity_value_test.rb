require "test_helper"

class EntityValueTest < ActiveSupport::TestCase
  test "should not save entity value without value" do
    entity_value = EntityValue.new
    assert_not entity_value.save, "Saved entity value without a value attribute"
  end
end
