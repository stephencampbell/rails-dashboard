# Rails Dashboard

### Tools
- Ruby 3.1.0
- Rails 7.0.1
- [Haml](https://haml.info)
- Sqlite
- [Hotwire](https://hotwired.dev)
- [Bulma](https://bulma.io)
- [FontAwesome](https://fontawesome.com)
