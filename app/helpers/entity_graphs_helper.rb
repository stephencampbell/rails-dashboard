module EntityGraphsHelper
  def eligible_fields(entity)
    entity.entity_fields.filter {|f| f.field_type and f.field_type.in? EntityGraph.valid_field_types }
  end

  def get_data_points(entity_graph)
    entity = entity_graph.entity
    entity_field_id = entity_graph.entity_field_id
    entity.entity_values.sort_by(&:entity_row_id).filter {|value| value.entity_field_id == entity_field_id}.map {|value| value.value.to_f}
  end

  def get_chart_labels(entity_graph)
    entity = entity_graph.entity
    if entity.primary_field
      field_id = entity.primary_field.id
    else
      field_id = entity.entity_fields.first.id
    end
    entity.entity_values.sort_by(&:entity_row_id).filter {|value| value.entity_field_id == field_id}.map {|value| value.value}
  end
end
