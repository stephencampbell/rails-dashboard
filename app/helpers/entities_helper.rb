module EntitiesHelper
  def all_entities
    Entity.all
  end

  def entity_values_by_row_and_field(entity)
    # Get prerequisite data
    fields = entity.entity_fields
    rows = entity.entity_rows
    values = entity.entity_values

    # Build structure
    rows_array = rows.each.map do |row|
      fields_array = fields.each.map do |field|
        value = values.find { |value| value.entity_field_id == field.id and value.entity_row_id == row.id }
        display_value = ''
        display_value = value.value if value

        [field.title, display_value]
      end
      [row.id, fields_array.to_h]
    end
    rows_array.to_h
  end
end
