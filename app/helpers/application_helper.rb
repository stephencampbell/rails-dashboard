module ApplicationHelper
  def active_page(path)
    'is-active' if current_page?(path)
  end
end
