module EntityValuesHelper
  def display_value_by_field_and_row(field:, row:)
    value = EntityValue.find_by(entity_field: field, entity_row: row)
    value.value if value
  end
end
