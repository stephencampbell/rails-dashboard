class EntityGraph < ApplicationRecord
  belongs_to :entity
  belongs_to :entity_field

  validates :graph_type, presence: true

  scope :graph_types_by_field_type, -> { GRAPH_TYPES_BY_FIELD_TYPE }
  scope :graph_types, -> { GRAPH_TYPES_BY_FIELD_TYPE.values.reduce {|arr, val| arr + val } }
  scope :valid_field_types, -> { GRAPH_TYPES_BY_FIELD_TYPE.keys.reduce {|arr, val| arr + val } }

  GRAPH_TYPES_BY_FIELD_TYPE = {
    'number' => [
      'bar',
      'line',
      'pie',
      'doughnut'
    ]
  }
end
