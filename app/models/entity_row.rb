class EntityRow < ApplicationRecord
  belongs_to :entity

  has_many :entity_values, dependent: :destroy
end
