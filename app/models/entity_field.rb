class EntityField < ApplicationRecord
  belongs_to :entity

  has_many :entity_values, dependent: :destroy
  has_many :entity_graphs, dependent: :destroy

  validates :title, presence: true
  validates :field_type, presence: true

  scope :field_types, -> { FIELD_TYPES }

  def is_primary_field? = entity.primary_field_id == id

  FIELD_TYPES = [
    'string',
    'number'
  ]
end
