class UnpopulatedRowError < StandardError; end

class Entity < ApplicationRecord
  has_many :entity_fields, dependent: :destroy
  has_many :entity_rows, dependent: :destroy
  has_many :entity_values, dependent: :destroy
  has_many :entity_graphs, dependent: :destroy
  has_one :primary_field, class_name: 'EntityField', primary_key: :primary_field_id, foreign_key: :id

  validates :title, presence: true
  before_destroy :clear_primary_field, prepend: true

  def clear_primary_field = update_attribute(:primary_field_id, nil)

  def update_row(row: nil, values_by_field: {})
    # Validate input is present and is associated with at least one field on this entity
    raise UnpopulatedRowError if values_by_field.values.length == 0
    raise UnpopulatedRowError unless entity_fields.any? {|field| values_by_field[field.title]}

    transaction do # Start a transaction so we can roll back if any values fail
      row = entity_rows.create! unless row # Create the row if needed

      entity_fields.each do |field| # For each field, attempt to update a value for the row
        value = values_by_field[field.title]
        next unless value # If no value has been submitted for this field, skip this iteration

        input = { entity_id: id, entity_field_id: field.id, entity_row_id: row.id }
        entity_value = EntityValue.find_by **input # Load the EntityValue for this field and row

        if entity_value and value.length == 0
          entity_value.destroy # If the value exists and new value is blank, clear it
        elsif entity_value
          entity_value.update! value: value # If we've found an EntityValue, update it
        elsif value.length > 0
          EntityValue.create! **input, value: value # Otherwise, make a new one
        end
      end

      # Validate there are still values for this row - cancel transaction if not
      values_for_row = EntityValue.where(entity_row_id: row.id)
      raise UnpopulatedRowError if values_for_row.length == 0

      row # Return the updated row
    end
  end
end
