class EntityValue < ApplicationRecord
  belongs_to :entity
  belongs_to :entity_field
  belongs_to :entity_row

  validates :value, presence: true
end
