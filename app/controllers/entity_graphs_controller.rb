class EntityGraphsController < ApplicationController
  before_action :get_entity, only: [:create, :destroy]
  before_action :get_entity_graph, only: [:destroy]

  def create
    @entity_graph = @entity.entity_graphs.new(entity_graph_params)

    if @entity_graph.save
      redirect_to entity_path(@entity)
    else
      redirect_to entity_path(@entity, error: 'Unable to add graph')
    end
  end

  def destroy
    @entity_graph.destroy
    redirect_to entity_path(@entity), status: 303
  end

  private

  def entity_graph_params = params.require(:entity_graph).permit(:graph_type, :entity_field_id)
  def get_entity = @entity = Entity.find(params[:entity_id])
  def get_entity_graph = @entity_graph = EntityGraph.find(params[:id])
end
