class EntityFieldsController < ApplicationController
  before_action :get_entity, only: [:create, :destroy, :edit, :update]
  before_action :get_entity_field, only: [:destroy, :edit, :update]

  def create
    @entity_field = @entity.entity_fields.new(entity_field_params)

    if @entity_field.save
      redirect_to entity_path(@entity)
    else
      redirect_to entity_path(@entity, error: 'Unable to add field')
    end
  end

  def destroy
    @entity.clear_primary_field if @entity_field.is_primary_field?

    @entity_field.destroy
    redirect_to entity_path(@entity), status: 303
  end

  def edit; end

  def update
    @entity_field.update(entity_field_params)
    redirect_to entity_path(@entity)
  end

  private

  def entity_field_params = params.require(:entity_field).permit(:title, :field_type)
  def get_entity = @entity = Entity.find(params[:entity_id])
  def get_entity_field = @entity_field = EntityField.find(params[:id])
end
