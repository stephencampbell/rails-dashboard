class EntitiesController < ApplicationController
  before_action :get_entity, only: [:show, :edit, :update, :destroy]

  def index = @entities = Entity.all

  def show; end

  def new = @entity = Entity.new

  def create
    @entity = Entity.new(entity_params)

    if @entity.save
      redirect_to entities_path
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit; end

  def update
    if @entity.update(entity_params)
      redirect_to @entity
    else
      render :new, status: :unprocessable_entity
    end
  end

  def destroy
    @entity.destroy
    redirect_to entities_path, status: 303
  end

  private

  def entity_params = params.require(:entity).permit(:title, :primary_field_id)
  def get_entity = @entity = Entity.find(params[:id])
end
