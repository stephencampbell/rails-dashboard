class EntityRowsController < ApplicationController
  before_action :get_entity, only: [:create, :destroy, :edit, :update]
  before_action :get_entity_row, only: [:destroy, :edit, :update]

  def create
    begin
      @entity.update_row(values_by_field: params[:entity_row].delete_if {|key, val| val.length == 0})
    rescue UnpopulatedRowError
      redirect_to entity_path(@entity, error: 'Unable to add row: row must have at least one value')
    else
      redirect_to entity_path(@entity)
    end
  end

  def destroy
    @entity_row.destroy
    redirect_to entity_path(@entity), status: 303
  end

  def edit; end

  def update
    begin
      @entity.update_row(row: @entity_row, values_by_field: params[:entity_row])
    rescue UnpopulatedRowError
      redirect_to entity_path(@entity, error: 'Unable to edit row: row must have at least one value')
    else
      redirect_to entity_path(@entity)
    end
  end

  private

  def get_entity = @entity = Entity.find(params[:entity_id])
  def get_entity_row = @entity_row = EntityRow.find(params[:id])
end
