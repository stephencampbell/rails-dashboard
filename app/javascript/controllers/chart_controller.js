import { Controller } from '@hotwired/stimulus'

export default class extends Controller {
  static values = {
    title: String,
    graphType: String,
    data: Array,
    labels: Array
  }
  static targets = ['canvas']
  colours = [
    '#ff520b',
    '#f5940b',
    '#85be47',
    '#1babfb',
    '#5582fc',
    '#aa67fa',
    '#d357fe'
  ]
  chart = null

  connect() {
    const data = {
      labels: this.labelsValue,
      datasets: [{
        label: this.titleValue,
        backgroundColor: this.colours,
        borderColor: this.colours,
        data: this.dataValue,
      }]
    }

    const config = {
      type: this.graphTypeValue,
      data: data,
      options: {}
    }

    this.chart = new Chart(
      this.canvasTarget,
      config
    )
  }
}
