import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  static targets = ['sidebar']
  static classes = ['closed']

  toggle() {
    this.sidebarTarget.classList.toggle(this.closedClass)
  }
}
