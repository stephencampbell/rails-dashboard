import { Controller } from '@hotwired/stimulus'

export default class extends Controller {
  // Stimulus properties
  static targets = ['table', 'body', 'row', 'sortIcon']
  static classes = ['noSort', 'sortAsc', 'sortDesc']
  static values = {
    sortField: String,
    sortDirection: String,
    filter: String,
    filterString: String
  }

  // Instance variables
  matchedRows = []

  // Actions
  updateSort(e) {
    if(this.sortFieldValue === e.target.dataset.sortField) return this.sortDirectionValue = this.sortDirectionValue === 'desc' ? 'asc' : 'desc'
    this.sortDirectionValue = 'asc'
    this.sortFieldValue = e.target.dataset.sortField
  }
  updateFilter = e => this.filterValue = e.target.value

  // Events
  sortFieldValueChanged = () => this.sort()
  sortDirectionValueChanged = () => this.sort()
  filterValueChanged = val => this.filterStringValue = val.toLowerCase()
  filterStringValueChanged = () => this.filter()
  matchedRowsChanged = () => this.highlightMatchedStrings()

  // Sorting methods
  sort() {
    this.updateSortIcons()

    if(!this.sortDirectionValue.length || !this.sortFieldValue.length) return
    const sortedRows = this.rowTargets.sort((a, b) => this.rowCompare(a, b))
    sortedRows.forEach(tr => this.bodyTarget.appendChild(tr))
  }
  updateSortIcons() {
    this.sortIconTargets.forEach(sortIcon => {
      sortIcon.classList.remove(this.noSortClass, this.sortAscClass, this.sortDescClass)
      if(sortIcon.dataset.sortField === this.sortFieldValue) return sortIcon.classList.add(this.sortDirectionValue === 'asc' ? this.sortAscClass : this.sortDescClass)
      sortIcon.classList.add(this.noSortClass)
    })
  }
  rowCompare(a, b) {
    if(this.sortDirectionValue === 'desc') [a, b] = [b, a]
    return this.getCellValue(a).localeCompare(this.getCellValue(b), 'en-u-kn-true')
  }
  getCellValue = row => row.querySelector(`[data-field="${this.sortFieldValue}"]`).dataset.value

  // Filtering methods
  filter() {
    this.updateMatchedRows()

    if(!this.matchedRows.length) return this.tableTarget.style.display = 'none'
    this.tableTarget.style.display = 'table'

    this.rowTargets.forEach(r => r.style.display = this.matchedRows.includes(r) ? 'table-row' : 'none')
  }
  updateMatchedRows() {
    this.matchedRows = this.rowTargets.filter(r => [...r.querySelectorAll('td[data-value]')].some(c => c.dataset.value.toLowerCase().includes(this.filterStringValue)))
    this.matchedRowsChanged()
  }
  highlightMatchedStrings() {
    this.matchedRows.forEach(r => {
      const cells = [...r.querySelectorAll('td[data-value]')]
      const matchingCells = cells.filter(c => c.dataset.value.toLowerCase().includes(this.filterStringValue))

      cells.forEach(cell => cell.innerHTML = cell.dataset.value)
      if(!this.filterStringValue.length) return
      matchingCells.forEach(c => c.innerHTML = this.getHighlightSpan(c.dataset.value))
    })
  }
  getHighlightSpan(val) {
    const index = val.toLowerCase().indexOf(this.filterStringValue)
    const searchLength = this.filterStringValue.length
    return `${val.slice(0, index)}<span class="is-underlined">${val.slice(index, index + searchLength)}</span>${val.slice(index + searchLength)}`
  }
}
