Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "pages#home"

  resources :entities do
    resources :entity_fields
    resources :entity_rows
    resources :entity_values
    resources :entity_graphs
  end
end
