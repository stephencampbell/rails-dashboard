# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_01_23_214816) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "entities", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "primary_field_id"
  end

  create_table "entity_fields", force: :cascade do |t|
    t.string "title"
    t.string "field_type"
    t.bigint "entity_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["entity_id"], name: "index_entity_fields_on_entity_id"
  end

  create_table "entity_graphs", force: :cascade do |t|
    t.bigint "entity_id", null: false
    t.bigint "entity_field_id", null: false
    t.string "graph_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["entity_field_id"], name: "index_entity_graphs_on_entity_field_id"
    t.index ["entity_id"], name: "index_entity_graphs_on_entity_id"
  end

  create_table "entity_rows", force: :cascade do |t|
    t.bigint "entity_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["entity_id"], name: "index_entity_rows_on_entity_id"
  end

  create_table "entity_values", force: :cascade do |t|
    t.bigint "entity_id", null: false
    t.bigint "entity_field_id", null: false
    t.bigint "entity_row_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "value"
    t.index ["entity_field_id"], name: "index_entity_values_on_entity_field_id"
    t.index ["entity_id"], name: "index_entity_values_on_entity_id"
    t.index ["entity_row_id"], name: "index_entity_values_on_entity_row_id"
  end

  add_foreign_key "entities", "entity_fields", column: "primary_field_id"
  add_foreign_key "entity_fields", "entities"
  add_foreign_key "entity_graphs", "entities"
  add_foreign_key "entity_graphs", "entity_fields"
  add_foreign_key "entity_rows", "entities"
  add_foreign_key "entity_values", "entities"
  add_foreign_key "entity_values", "entity_fields"
  add_foreign_key "entity_values", "entity_rows"
end
