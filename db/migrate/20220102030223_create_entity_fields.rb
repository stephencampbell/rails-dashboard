class CreateEntityFields < ActiveRecord::Migration[7.0]
  def change
    create_table :entity_fields do |t|
      t.string :title
      t.string :type
      t.references :entity, null: false, foreign_key: true

      t.timestamps
    end
  end
end
