class ChangeEntityFieldsTypeToFieldType < ActiveRecord::Migration[7.0]
  def change
    rename_column :entity_fields, :type, :field_type
  end
end
