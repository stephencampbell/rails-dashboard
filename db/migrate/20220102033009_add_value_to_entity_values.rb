class AddValueToEntityValues < ActiveRecord::Migration[7.0]
  def change
    add_column :entity_values, :value, :string
  end
end
