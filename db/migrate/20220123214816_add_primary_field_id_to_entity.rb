class AddPrimaryFieldIdToEntity < ActiveRecord::Migration[7.0]
  def change
    add_column :entities, :primary_field_id, :integer
    add_foreign_key :entities, :entity_fields, column: :primary_field_id
  end
end
