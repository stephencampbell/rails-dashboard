class CreateEntityValues < ActiveRecord::Migration[7.0]
  def change
    create_table :entity_values do |t|
      t.references :entity, null: false, foreign_key: true
      t.references :entity_field, null: false, foreign_key: true
      t.references :entity_row, null: false, foreign_key: true

      t.timestamps
    end
  end
end
