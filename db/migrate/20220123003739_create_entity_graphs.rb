class CreateEntityGraphs < ActiveRecord::Migration[7.0]
  def change
    create_table :entity_graphs do |t|
      t.references :entity, null: false, foreign_key: true
      t.references :entity_field, null: false, foreign_key: true
      t.string :graph_type

      t.timestamps
    end
  end
end
